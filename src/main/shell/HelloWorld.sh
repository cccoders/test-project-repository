# Copyright (C) 2013,  Climate Change Coders
# 
# This file is part of Test System.
# 
# Test System is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# Test System is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with Test System.  If not, see <http://www.gnu.org/licenses/>.
#
# 
# This shell script prints "Hello World".
#
# Usage:
# ./HelloWorld.sh
#

date;
echo "Starting HelloWorld.sh"
echo "Hello World";
date;
echo "Finished HelloWorld.sh"
