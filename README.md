This is the README file for the Test Project in JIRA - see https://cccoders.atlassian.net/browse/TP.

LICENSE

Test System is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Test System is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Test System should include a copy of the GNU General Public License.  If not, see <http://www.gnu.org/licenses/>.
